# CERadarView

[![CI Status](http://img.shields.io/travis/ekozlov89/CERadarView.svg?style=flat)](https://travis-ci.org/ekozlov89/CERadarView)
[![Version](https://img.shields.io/cocoapods/v/CERadarView.svg?style=flat)](http://cocoapods.org/pods/CERadarView)
[![License](https://img.shields.io/cocoapods/l/CERadarView.svg?style=flat)](http://cocoapods.org/pods/CERadarView)
[![Platform](https://img.shields.io/cocoapods/p/CERadarView.svg?style=flat)](http://cocoapods.org/pods/CERadarView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CERadarView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CERadarView'
```

## Author

ekozlov89, ekozlov89@mail.ru

## License

CERadarView is available under the MIT license. See the LICENSE file for more info.
