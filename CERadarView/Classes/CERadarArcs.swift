//
//  Arcs.swift
//  FlightTracker
//
//  Created by Евгений on 07.10.16.
//  Copyright © 2016 Евгений. All rights reserved.
//

import UIKit

class CERadarArcs: UIView {

    var lineColor: UIColor = UIColor.black.withAlphaComponent(0.5) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var lineWidth: CGFloat = 1.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var lineLastWidth: CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var lineCount: Int = 3 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var lineCenterOffset: CGFloat = 0.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var centerRadius: CGFloat = 2.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var centerColor: UIColor = UIColor.black.withAlphaComponent(0.5) {
        didSet {
            self.setNeedsDisplay()
        }
    }
     
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isOpaque = false
        self.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.isOpaque = false
        self.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        self.setNeedsDisplay()
        self.layer.cornerRadius = min(bounds.size.width, bounds.size.height) / 2.0
    }
    
    override func draw(_ rect: CGRect) {

        let centerPoint: CGPoint = CGPoint(x: rect.size.width / 2.0, y: rect.size.height / 2.0)
        let increment: CGFloat = (min(rect.size.width, rect.size.height) / 2.0 - lineCenterOffset) / CGFloat(lineCount)
        
        for i in 1...lineCount {
            let radius: CGFloat = CGFloat(i) * increment - ((i == lineCount) ? lineLastWidth : lineWidth) / 2.0
            
            let linePath: UIBezierPath = UIBezierPath(arcCenter: centerPoint, radius: radius, startAngle: 0.0, endAngle: 2.0 * CGFloat.pi, clockwise: true)
            
            linePath.lineWidth = (i == lineCount) ? lineLastWidth : lineWidth

            lineColor.setStroke()
            linePath.stroke()
        }
        
        centerColor.setFill()
        let centerPath: UIBezierPath = UIBezierPath(ovalIn: CGRect(x: centerPoint.x - centerRadius, y: centerPoint.y - centerRadius, width: 2 * centerRadius, height: 2 * centerRadius))
        centerPath.fill()
    }

    func degreesToRadiands(degrees: CGFloat) -> CGFloat {
        return CGFloat.pi * degrees / 180
    }
}
