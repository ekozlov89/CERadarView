//
//  Radar.swift
//  FlightTracker
//
//  Created by Евгений on 11.10.16.
//  Copyright © 2016 Евгений. All rights reserved.
//

import UIKit

class CERadar: UIView {

    var color = UIColor.green {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.isOpaque = false
        self.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.isOpaque = false
        self.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.height / 2
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        for i in 0...180 {
            let shadowColor = color.withAlphaComponent(CGFloat(i) / 360).cgColor
            context?.setFillColor(shadowColor)
            context?.move(to: CGPoint(x: rect.width / 2.0, y: rect.height / 2.0))
            context?.addArc(center: CGPoint(x: rect.width / 2.0, y: rect.height / 2.0), radius: rect.height / 2.0, startAngle: (-180 + CGFloat(i)) / 180 * CGFloat.pi, endAngle: (-180 + CGFloat(i) - 1) / 180 * CGFloat.pi, clockwise: true)
            context?.closePath()
            context?.drawPath(using: CGPathDrawingMode.fill)
        }

        context?.setStrokeColor(color.withAlphaComponent(0.9).cgColor)
        context?.move(to: CGPoint(x: rect.width / 2.0, y: rect.height / 2.0))
        context?.addLine(to: CGPoint(x: rect.width, y: rect.height / 2.0))
        context?.strokePath()

    }

}
