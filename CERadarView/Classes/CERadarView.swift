//
//  RadarView.swift
//  FlightTracker
//
//  Created by Евгений on 11.10.16.
//  Copyright © 2016 Евгений. All rights reserved.
//

import UIKit
import MapKit

@IBDesignable
open class CERadarView: UIView {

    let arcs: CERadarArcs = CERadarArcs()
    let line: CERadarLine = CERadarLine()
    let radar: CERadar = CERadar()
    
    @IBInspectable public var radarColor: UIColor {
        get {
            return radar.color
        }
        set {
            radar.color = newValue
            line.backgroundColor = newValue
        }
    }
    
    @IBInspectable public var arcLineCount: Int {
        get {
            return arcs.lineCount
        }
        set {
            arcs.lineCount = newValue
        }
    }
    @IBInspectable public var arcLineWidth: CGFloat {
        get {
            return arcs.lineWidth
        }
        set {
            arcs.lineWidth = newValue
        }
    }
    @IBInspectable public var arcLineColor: UIColor {
        get {
            return arcs.lineColor
        }
        set {
            arcs.lineColor = newValue
        }
    }
    
    @IBInspectable public var dotSize: CGSize = CGSize(width: 4.0, height: 4.0) {
        didSet {
            updateDots()
        }
    }
    @IBInspectable public var dotColor: UIColor = UIColor.black {
        didSet {
            updateDots()
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable public var borderColor: UIColor? {
        get {
            return layer.borderColor.flatMap({ UIColor(cgColor: $0) })
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable public var maxDistance: Double = 100000 {
        didSet {
            updateDots()
        }
    }

    @IBInspectable public var showLine: Bool = true {
        didSet {
            reloadAnimation()
        }
    }
    
    @IBInspectable public var animate: Bool = true {
        didSet {
            reloadAnimation()
        }
    }
    
    public var userCoordinate: CLLocationCoordinate2D! = CLLocationCoordinate2D() {
        didSet {
            updateDots()
        }
    }
    
    var dotsView: [CERadarDotView] = []
    public var dots: [CERadarDot] = [] {
        didSet {
            self.updateDots()
        }
    }
    
    var currentDeviceBearing: Double = 0.0 {
        didSet {
            let headingAngle: Double = -(currentDeviceBearing * Double.pi / 180)
            rotateArcsToHeading(angle: headingAngle)
        }
    }
    
    var detectCollisionTimer: Timer! = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        #if !TARGET_INTERFACE_BUILDER
        initialize()
        #endif
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        initialize()
    }
    
    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        initialize()
        #if TARGET_INTERFACE_BUILDER
        addCustomDots()
        #endif
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        if (currentDeviceBearing == 0) {
            self.layer.cornerRadius = min(bounds.size.width, bounds.size.height) / 2.0
            
            arcs.frame = CGRect(x: borderWidth, y: borderWidth, width: bounds.size.width - 2 * borderWidth, height: bounds.size.height - 2 * borderWidth)
            radar.frame = CGRect(x: borderWidth, y: borderWidth, width: bounds.size.width - 2 * borderWidth, height: bounds.size.height - 2 * borderWidth)
            line.frame = CGRect(x: bounds.width / 2.0, y: bounds.height / 2.0, width: bounds.width / 2.0 - borderWidth, height: 1.0)
        }
    }
    
    func initialize() {
        self.isOpaque = false
        self.layer.masksToBounds = true

        arcs.lineCount = arcLineCount
        arcs.centerRadius = 0.0
        arcs.lineColor = arcLineColor
        arcs.lineWidth = arcLineWidth
        self.addSubview(arcs)
        
        radar.color = radarColor
        self.addSubview(radar)
        
        line.backgroundColor = radarColor
        self.addSubview(line)
        
        reloadAnimation()
    }
    
    func reloadAnimation() {
        line.layer.removeAllAnimations()
        radar.layer.removeAllAnimations()
        
        line.alpha = showLine ? 1.0 : 0.0
        radar.alpha = showLine && animate ? 1.0 : 0.0
        if (showLine && animate) {
            spinRadar()
            if (detectCollisionTimer == nil) {
                #if !TARGET_INTERFACE_BUILDER
                detectCollisionTimer = Timer.scheduledTimer(timeInterval: 2.0 / 36.0, target: self, selector: #selector(detectCollisions(timer:)), userInfo: nil, repeats: true)
                #endif
            }
        }else {
            if (detectCollisionTimer != nil) {
                detectCollisionTimer.invalidate()
                detectCollisionTimer = nil
            }
        }
        
        updateDots()
    }

    func spinRadar() {
        let spin: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation")
        spin.duration = 2
        spin.toValue = 2 * Double.pi
        spin.isCumulative = true
        spin.isRemovedOnCompletion = false
        spin.repeatCount = Float.infinity
        line.layer.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        
        line.layer.add(spin, forKey: "spinRadarLine")
        radar.layer.add(spin, forKey: "spinRadarView")
    }
    
    func updateDots() {
        removeDots()

        rendeDotsOnRadar(dots: dots)
    }
    
    func removeDots() {
        for dotView in dotsView {
            dotView.removeFromSuperview()
        }
        
//        dots.removeAll()
    }
    
    func rendeDotsOnRadar(dots: [CERadarDot]) {
        let radius: Double = Double(min(arcs.bounds.size.width, arcs.bounds.size.height)) / 2.0
        for dot in dots {
            let dotView: CERadarDotView = CERadarDotView(size: dotSize)
            
            dotView.dot = dot
            
            #if TARGET_INTERFACE_BUILDER
                dotView.layer.opacity = 1.0
            #else
                dotView.layer.opacity = animate ? 0.0 : 1.0
            #endif
            
            dotView.color = dotColor
            
            dotView.bearing = getHeadingForDirection(fromCoordinate: userCoordinate, toCoordinate: dot.coordinate)
            dotView.distance = userCoordinate.location().distance(from: dot.coordinate.location())
            dotView.length = dotView.distance * (radius / maxDistance)

            let position: CGPoint = CGPoint(x: radius + dotView.length * sin(degreesToRadians(degrees: dotView.bearing)), y: radius - dotView.length * cos(degreesToRadians(degrees: dotView.bearing)))
//            dotView.layer.position = position
            dotView.frame.origin = position
            
            arcs.addSubview(dotView)
            dotsView.append(dotView)
        }
    }
    
    @objc func detectCollisions(timer: Timer) {
        var radarLineRotation: Double = radiandsToDegrees(radiands: line.layer.presentation()?.value(forKeyPath: "transform.rotation.z") as! Double) + 90
        
        if (radarLineRotation < 0) {
            radarLineRotation += 360
        }

        for dotView in dotsView {
            var dotBearing = dotView.bearing - currentDeviceBearing
            if (dotBearing < 0) {
                dotBearing += 360
            }

            if (abs(dotBearing - radarLineRotation) <= 10) {
                pulse(dot: dotView)
            }
        }
    }
    
    func pulse(dot: CERadarDotView) {
        if (((dot.layer.animationKeys() != nil) && (dot.layer.animationKeys()!.contains("pulse"))) || (dot.zoomEnabled)) { return }
        
        let pulse: CABasicAnimation = CABasicAnimation(keyPath: "opacity")
        pulse.duration = 1.5
        pulse.fromValue = 1.0
        pulse.toValue = 0.0

        pulse.fillMode = kCAFillModeBoth
        pulse.isAdditive = false
        
        dot.layer.add(pulse, forKey: "pulse")
    }

    func getHeadingForDirection(fromCoordinate: CLLocationCoordinate2D, toCoordinate: CLLocationCoordinate2D) -> Double {
        let fromLat: Double = degreesToRadians(degrees: fromCoordinate.latitude)
        let fromLon: Double = degreesToRadians(degrees: fromCoordinate.longitude)
        let toLat: Double = degreesToRadians(degrees: toCoordinate.latitude)
        let toLon: Double = degreesToRadians(degrees: toCoordinate.longitude)
        
        let degree: Double = radiandsToDegrees(radiands: atan2(sin(toLon - fromLon) * cos(toLat), cos(fromLat) * sin(toLat) - sin(fromLat) * cos(toLat) * cos(toLon - fromLon)))
        if (degree >= 0) {
            return degree
        }else {
            return (360 + degree)
        }
    }
    
    public func addCustomDots() {
        var dots: [CERadarDot] = []
        for _ in 0...10 {
            let latitude: CLLocationDistance = CLLocation(latitude: 0.0, longitude: 0.0).distance(from: CLLocation(latitude: 1.0, longitude: 0.0))
            let longitude: CLLocationDistance = CLLocation(latitude: 0.0, longitude: 0.0).distance(from: CLLocation(latitude: 0.0, longitude: 1.0))
            
            let x: Double = Double(arc4random_uniform(UInt32(2 * maxDistance))) - maxDistance
            let left: Double = maxDistance - abs(x)
            let y: Double = Double(arc4random_uniform(UInt32(2 * left))) - left
            
            let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: x / latitude, longitude: y / longitude)
            dots.append(CERadarDot(coordinate: coordinate))
        }
        self.dots = dots
    }
    
    func rotateArcsToHeading(angle: Double) {
        arcs.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
    }
    
    func degreesToRadians(degrees: Double) -> Double {
        return  degrees * Double.pi / 180.0
    }
    
    func radiandsToDegrees(radiands: Double) -> Double {
        return radiands * 180 / Double.pi
    }
}

extension CLLocationCoordinate2D {
    
    func location() -> CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
}
