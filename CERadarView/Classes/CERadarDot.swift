//
//  Dot.swift
//  FlightTracker
//
//  Created by Евгений on 11.10.16.
//  Copyright © 2016 Евгений. All rights reserved.
//

import UIKit
import MapKit

public struct CERadarDot {
    
    let coordinate: CLLocationCoordinate2D
    
}

class CERadarDotView: UIView {

    var color: UIColor? {
        get {
            return backgroundColor
        }
        set {
            backgroundColor = newValue
        }
    }
    
    var dot: CERadarDot!
    
    var bearing: Double = 0
    var distance: Double = 0
    var length: Double = 0

    var zoomEnabled: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    convenience init(size: CGSize) {
        self.init(frame: CGRect(origin: CGPoint.zero, size: size))
    }
    
    func initialize() {
        self.isOpaque = false
        self.isUserInteractionEnabled = false
        self.backgroundColor = color

        let shapeLayer: CAShapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(ovalIn: self.bounds).cgPath
        self.layer.mask = shapeLayer
        self.layer.masksToBounds = true
    }

}
